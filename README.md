# README #


### Sound Output Switch ###

* If you are lazy like me to chase the Volume icon to click twice to change sound output you may like this script :)
* Ver 0.1

### How do set it up? ###

* Apple script for use with SwitchAudioSource
* First install https://github.com/deweller/switchaudio-osx
* Then Download this AppleScript
* Configure the path to where you installed SwitchAudioSource
* Configure the names of two outputs you want to switch between, do the list from command line using switchaudiosource -a. Use the exact name of the desired output device.
* Export AppleScript as App
* Drag the app to dock and that is it :)
* If you like, change the default icon of the app

